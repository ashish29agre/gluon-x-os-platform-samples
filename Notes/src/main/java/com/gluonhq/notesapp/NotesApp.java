package com.gluonhq.notesapp;

import com.gluonhq.notesapp.views.NotesView;
import com.gluonhq.notesapp.views.EditionView;
import com.gluonhq.charm.glisten.application.MobileApplication;
import com.gluonhq.charm.glisten.control.Avatar;
import com.gluonhq.charm.glisten.control.NavigationDrawer;
import com.gluonhq.charm.glisten.control.NavigationDrawer.Item;
import com.gluonhq.charm.glisten.layout.layer.SidePopupView;
import com.gluonhq.charm.glisten.mvc.View;
import com.gluonhq.charm.glisten.visual.MaterialDesignIcon;
import com.gluonhq.charm.glisten.visual.Swatch;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.stage.Stage;
import com.gluonhq.notesapp.views.SettingsView;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import javafx.beans.value.ChangeListener;
import javafx.scene.Node;

public class NotesApp extends MobileApplication {

    public static final String NOTES_VIEW = HOME_VIEW;
    public static final String EDITION_VIEW = "Edition View";
    public static final String SETTINGS_VIEW = "Settings View";
    public static final String DRAWER = "Side Menu";
    public static final String POPUP_FILTER_NOTES = "Filter Notes";
    
    private final Map<String, String> itemsMap = new HashMap<>();
    
    private final ChangeListener<Node> listener = (obs, oldItem, newItem) -> {
            hideLayer(DRAWER);
            Iterator<Map.Entry<String, String>> iterator = itemsMap.entrySet().iterator();
            while (iterator.hasNext()) {
                final Map.Entry<String, String> next = iterator.next();
                if (next.getValue().equals(((Item) newItem).getTitle())) {
                    switchView(next.getKey());
                    break;
                }
            }
        };
    
    @Override
    public void init() {
        
        addViewFactory(NOTES_VIEW, () -> (View) new NotesView().getView());
        addViewFactory(EDITION_VIEW, () -> (View) new EditionView().getView());
        addViewFactory(SETTINGS_VIEW, () -> (View) new SettingsView().getView());
        
        NavigationDrawer drawer = new NavigationDrawer();
        
        NavigationDrawer.Header header = new NavigationDrawer.Header("Gluon Mobile",
                "Notes App Plus",
                new Avatar(21, new Image(NotesApp.class.getResourceAsStream("/icon.png"))));
        drawer.setHeader(header);
        
        itemsMap.put(NOTES_VIEW, "Notes");
        itemsMap.put(EDITION_VIEW, "Edition");
        itemsMap.put(SETTINGS_VIEW, "Settings");
        
        final Item primaryItem = new Item(itemsMap.get(NOTES_VIEW), MaterialDesignIcon.HOME.graphic());
        final Item secondaryItem = new Item(itemsMap.get(EDITION_VIEW), MaterialDesignIcon.DASHBOARD.graphic());
        final Item settingsItem = new Item(itemsMap.get(SETTINGS_VIEW), MaterialDesignIcon.SETTINGS.graphic());
        drawer.getItems().addAll(primaryItem, secondaryItem, settingsItem);
        
        addLayerFactory(DRAWER, () -> new SidePopupView(drawer));
        
        primaryItem.setSelected(true);
        drawer.selectedItemProperty().addListener(listener);
        
        viewProperty().addListener((obs, ov, nv) -> {
            for (Node node : drawer.getItems()) {
                NavigationDrawer.Item item = (NavigationDrawer.Item) node;
                if (itemsMap.get(nv.getName()).equals(item.getTitle())) {
                    drawer.selectedItemProperty().removeListener(listener);
                    drawer.setSelectedItem(node);
                    item.setSelected(true);
                    drawer.selectedItemProperty().addListener(listener);
                } else {
                    item.setSelected(false);
                }
            }
        });
    }

    @Override
    public void postInit(Scene scene) {
        Swatch.LIGHT_GREEN.assignTo(scene);

        scene.getStylesheets().add(NotesApp.class.getResource("style.css").toExternalForm());
        ((Stage) scene.getWindow()).getIcons().add(new Image(NotesApp.class.getResourceAsStream("/icon.png")));
    }
}
