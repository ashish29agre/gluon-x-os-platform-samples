package com.gluonhq.rubik;

import com.gluonhq.charm.down.common.JavaFXPlatform;
import com.gluonhq.rubik.views.SplashView;
import com.gluonhq.rubik.views.RubikView;
import com.gluonhq.charm.glisten.application.MobileApplication;
import com.gluonhq.charm.glisten.visual.Swatch;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.stage.Stage;
import javafx.geometry.Rectangle2D;
import javafx.stage.Screen;

public class GluonRubik extends MobileApplication {

    public static final String SPLASH_VIEW = HOME_VIEW;
    public static final String RUBIK_VIEW = "Rubik View";
    
    @Override
    public void init() {
        addViewFactory(SPLASH_VIEW, () -> new SplashView(SPLASH_VIEW));
        addViewFactory(RUBIK_VIEW, () -> new RubikView(RUBIK_VIEW));
    }

    @Override
    public void postInit(Scene scene) {
        Swatch.TEAL.assignTo(scene);

        if (JavaFXPlatform.isDesktop()) {
            ((Stage) scene.getWindow()).getIcons().add(new Image(GluonRubik.class.getResourceAsStream("/icon.png")));
            Rectangle2D visualBounds = Screen.getPrimary().getVisualBounds();
            ((Stage) scene.getWindow()).setWidth(visualBounds.getWidth());
            ((Stage) scene.getWindow()).setHeight(visualBounds.getHeight());
        }
    }
}
